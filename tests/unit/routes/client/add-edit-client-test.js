import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | client/add-edit-client', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:client/add-edit-client');
    assert.ok(route);
  });
});
