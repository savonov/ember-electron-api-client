import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | contract/index', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:contract/index');
    assert.ok(route);
  });
});
