import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | contract/add-edit-contract', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:contract/add-edit-contract');
    assert.ok(route);
  });
});
