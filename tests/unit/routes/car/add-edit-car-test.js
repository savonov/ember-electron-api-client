import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | car/add-edit-car', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:car/add-edit-car');
    assert.ok(route);
  });
});
