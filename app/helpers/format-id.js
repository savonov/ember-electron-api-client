import {
  helper
} from '@ember/component/helper';

export function formatId(params /*, hash*/ ) {
  return params.toFixed(6);
}

export default helper(formatId);
