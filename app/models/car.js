import DS from 'ember-data';

export default DS.Model.extend({
  _id: DS.attr('string'),
  brand: DS.attr('string'),
  state: DS.attr('number'),
  availability: DS.attr('boolean')
});
