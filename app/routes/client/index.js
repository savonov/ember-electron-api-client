import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    return [{
        _id: "5bd6c66447cc892127f443ec",
        FIO: "Павел",
        passport: "83838383",
        tel: "084883833"
      },
      {
        _id: "5bd785c8a059bd5bd623bcf0",
        FIO: "Дима",
        passport: "72828",
        tel: "0111929291"
      },
      {
        _id: "5bd805cafc5186250b7bf5b5",
        FIO: "Dimooooooooon",
        passport: "321312312312",
        tel: "323232"
      },
      {
        _id: "5bd8126fc075471c2d527082",
        FIO: "Yarik",
        passport: "1234578",
        tel: "0121212"
      }
    ]
  }
});
