import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    return [{
        _id: "5bc5971d31d4e75145e30a46",
        brand: "Audi",
        state: 4,
        availability: true
      },
      {
        _id: "5bc597f931d4e75145e30a47",
        brand: "BMW",
        state: 4,
        color: null,
        availability: true
      },
      {
        _id: "5bd805bafc5186250b7bf5b4",
        brand: "lamba",
        state: 4,
        availability: false
      }
    ]
  }
});
