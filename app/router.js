import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function () {
  this.route('about');
  this.route('contact');
  this.route('car/index');
  this.route('client/index');
  this.route('contract/index');
  this.route('car/add-edit-car');
  this.route('client/add-edit-client');
  this.route('contract/add-edit-contract');
});

export default Router;
